package hk.brianli.contact_list;

import java.io.Serializable;

/**
 * Created by lima1 on 10.07.2017.
 */

public class Person implements Serializable{
    private String name;
    private boolean male;
    private String address;
    private int phone;
    private String nickname;
    private String email;

    public Person(String name,int phone){
        this.name=name;
        this.phone=phone;
    }

    public Person(String name){
        this.name=name;
    }

    public Person(String name,boolean male,int phone){
        this.name=name;
        this.male=male;
        this.phone=phone;
    }


    public void setName(String name){
        this.name=name;
    }

    public String getName(){
        return this.name;
    }

    public void isMale(boolean gender){
        this.male=gender;
    }

    public boolean isMale() {
        return male;
    }

    public void setAddr(String address){
        this.address=address;
    }

    public String getAddr(){
        return this.address;
    }

    public void setPhone(){
        this.phone=phone;
    }

    public int getPhone(){
        return this.phone;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String toString(){
        return this.name;
    }

}
