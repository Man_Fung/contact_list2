package hk.brianli.contact_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView nickname=(TextView)findViewById(R.id.nickname);
        TextView phone=(TextView)findViewById(R.id.phone);
        TextView fullname=(TextView)findViewById(R.id.name);
        TextView email=(TextView)findViewById(R.id.email);
        TextView address=(TextView)findViewById(R.id.address);

        ImageView image=(ImageView)findViewById(R.id.imageView);

        Person person=(Person) getIntent().getSerializableExtra("person");

        nickname.setText(person.getNickname());
        email.setText(person.getEmail());
        phone.setText(String.valueOf(person.getPhone()));
        fullname.setText(person.getName());
        address.setText(person.getAddr());

        if (person.isMale())
            image.setImageResource(R.drawable.men);
        else
            image.setImageResource(R.drawable.women);

    }
}
