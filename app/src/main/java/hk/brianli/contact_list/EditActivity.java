package hk.brianli.contact_list;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class EditActivity extends AppCompatActivity {
        Person newPerson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Button addBtn=(Button)findViewById(R.id.add);
        final EditText name=(EditText)findViewById(R.id.name);
        final EditText nickName=(EditText)findViewById(R.id.nickname);
        final EditText email=(EditText)findViewById(R.id.email);
        final EditText address=(EditText)findViewById(R.id.address);
        final EditText phone=(EditText)findViewById(R.id.phone);
        final CheckBox male=(CheckBox)findViewById(R.id.male);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                newPerson=new Person(name.getText().toString(),male.isChecked(),Integer.parseInt(phone.getText().toString()));
                newPerson.setEmail(email.getText().toString());
                newPerson.setNickname(nickName.getText().toString());
                newPerson.setAddr(address.getText().toString());
                Intent intent=new Intent();
                intent.putExtra("result",newPerson);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });

    }
}
