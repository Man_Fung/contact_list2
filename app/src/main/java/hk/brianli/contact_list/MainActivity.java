package hk.brianli.contact_list;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

        private ArrayList<Person> persons;

        private ArrayAdapter<Person> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        persons=new ArrayList<>();



        final Person sunny=new Person("Chan Tin Nam",true,123456498);
        sunny.setEmail("sunny@abc.com");
        sunny.setNickname("Sunny");
        sunny.setAddr("Fucking University");



        final Person kingston=new Person("Lui ting Yat",true,123456498);
        kingston.setEmail("luifatfat@gamil.com");
        kingston.setNickname("Kingston");
        kingston.setAddr("Fucking University Dormistry");



        final Person matthew=new Person("Lai Wai Ki",false,123456498);
        matthew.setEmail("matthew@abc.com");
        matthew.setNickname("Matthew");
        matthew.setAddr("Fucking University");

        final Person brian=new Person("Li Man Fung",true,123456498);
        brian.setEmail("brianli@abc.com");
        brian.setNickname("Fattyboy");
        brian.setAddr("Fucking University");

        persons.add(sunny);
        persons.add(brian);
        persons.add(matthew);
        persons.add(kingston);
        persons.add(new Person("Tom",true,323203));

        ListView listViewPersons=(ListView)findViewById(R.id.listViewPerson);
        adapter=new ArrayAdapter<Person>(this,android.R.layout.simple_list_item_1,persons);
        listViewPersons.setAdapter(adapter);

        listViewPersons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                intent.putExtra("person",persons.get(i));
                startActivity(intent);
            }
        });

        listViewPersons.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
        final AlertDialog show = builder.setTitle("Delete Contact")
                .setMessage("Are you sure you want to delete seven head "+persons.get(i)+"?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        persons.remove(i);
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

        return true;
    }
});

      /*  Button sunnyBtn= (Button) findViewById(R.id.sunny);
        sunnyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                intent.putExtra("person",persons.get(0));
                startActivity(intent);
            }
        });

        Button brianBtn= (Button) findViewById(R.id.brian);
        brianBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                intent.putExtra("person",persons.get(1));
                startActivity(intent);
            }
        });

        Button kingstonBtn= (Button) findViewById(R.id.kingston);
        kingstonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                intent.putExtra("person",persons.get(3));
                startActivity(intent);
            }
        });

        Button matthewBtn= (Button) findViewById(R.id.matthew);
        matthewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                intent.putExtra("person",persons.get(2));
                startActivity(intent);
            }
        });


        */
        FloatingActionButton add = (FloatingActionButton) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,EditActivity.class);
                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            if (resultCode == Activity.RESULT_OK){
                Person newPerson = (Person) data.getSerializableExtra("result");
                persons.add(newPerson);
                adapter.notifyDataSetChanged();
            }
        }

    }
}
